EESchema Schematic File Version 4
LIBS:HP8903-3.96-30-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x15_Odd_Even J3
U 1 1 5FD42199
P 4250 2575
F 0 "J3" H 4300 3475 50  0000 C CNN
F 1 "CONN-180" H 4300 3375 50  0000 C CNN
F 2 "suf_connector_edge:EDGE-3.96-30" H 4250 2575 50  0001 C CNN
F 3 "~" H 4250 2575 50  0001 C CNN
	1    4250 2575
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x15_Odd_Even J4
U 1 1 5FD421CF
P 5350 2575
F 0 "J4" H 5400 3475 50  0000 C CNN
F 1 "CONN-90" H 5400 3375 50  0000 C CNN
F 2 "suf_connector_edge:CONN-3.96-30" H 5350 2575 50  0001 C CNN
F 3 "~" H 5350 2575 50  0001 C CNN
	1    5350 2575
	1    0    0    -1  
$EndComp
Entry Wire Line
	1575 1975 1675 1875
Text Label 1700 1875 0    50   ~ 0
1
Entry Wire Line
	1575 2075 1675 1975
Text Label 1700 1975 0    50   ~ 0
3
Entry Wire Line
	1575 2175 1675 2075
Text Label 1700 2075 0    50   ~ 0
5
Entry Wire Line
	1575 2275 1675 2175
Text Label 1700 2175 0    50   ~ 0
7
Entry Wire Line
	1575 2375 1675 2275
Text Label 1700 2275 0    50   ~ 0
9
Entry Wire Line
	1575 2475 1675 2375
Text Label 1700 2375 0    50   ~ 0
11
Entry Wire Line
	1575 2575 1675 2475
Text Label 1700 2475 0    50   ~ 0
13
Entry Wire Line
	1575 2675 1675 2575
Text Label 1700 2575 0    50   ~ 0
15
Entry Wire Line
	1575 2775 1675 2675
Text Label 1700 2675 0    50   ~ 0
17
Entry Wire Line
	1575 2875 1675 2775
Text Label 1700 2775 0    50   ~ 0
19
Entry Wire Line
	1575 2975 1675 2875
Text Label 1700 2875 0    50   ~ 0
21
Entry Wire Line
	1575 3075 1675 2975
Text Label 1700 2975 0    50   ~ 0
23
Entry Wire Line
	1575 3175 1675 3075
Text Label 1700 3075 0    50   ~ 0
25
Text Label 1700 3175 0    50   ~ 0
27
$Comp
L Connector_Generic:Conn_02x15_Odd_Even J1
U 1 1 5FD30FBA
P 2050 2575
F 0 "J1" H 2100 3475 50  0000 C CNN
F 1 "EDGE" H 2100 3375 50  0000 C CNN
F 2 "suf_connector_edge:EDGE-3.96-30" H 2050 2575 50  0001 C CNN
F 3 "~" H 2050 2575 50  0001 C CNN
	1    2050 2575
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 1875 1850 1875
Wire Wire Line
	1675 1975 1850 1975
Wire Wire Line
	1675 2075 1850 2075
Wire Wire Line
	1675 2175 1850 2175
Wire Wire Line
	1675 2275 1850 2275
Wire Wire Line
	1675 2375 1850 2375
Wire Wire Line
	1675 2475 1850 2475
Wire Wire Line
	1675 2575 1850 2575
Wire Wire Line
	1675 2675 1850 2675
Wire Wire Line
	1675 2775 1850 2775
Wire Wire Line
	1675 2875 1850 2875
Wire Wire Line
	1675 2975 1850 2975
Wire Wire Line
	1675 3075 1850 3075
Wire Wire Line
	1675 3175 1850 3175
Entry Wire Line
	2625 1975 2525 1875
Text Label 2500 1875 2    50   ~ 0
2
Entry Wire Line
	2625 2075 2525 1975
Text Label 2500 1975 2    50   ~ 0
4
Entry Wire Line
	2625 2175 2525 2075
Text Label 2500 2075 2    50   ~ 0
6
Entry Wire Line
	2625 2275 2525 2175
Text Label 2500 2175 2    50   ~ 0
8
Entry Wire Line
	2625 2375 2525 2275
Text Label 2500 2275 2    50   ~ 0
10
Entry Wire Line
	2625 2475 2525 2375
Text Label 2500 2375 2    50   ~ 0
12
Entry Wire Line
	2625 2575 2525 2475
Text Label 2500 2475 2    50   ~ 0
14
Entry Wire Line
	2625 2675 2525 2575
Text Label 2500 2575 2    50   ~ 0
16
Entry Wire Line
	2625 2775 2525 2675
Text Label 2500 2675 2    50   ~ 0
18
Entry Wire Line
	2625 2875 2525 2775
Text Label 2500 2775 2    50   ~ 0
20
Entry Wire Line
	2625 2975 2525 2875
Text Label 2500 2875 2    50   ~ 0
22
Entry Wire Line
	2625 3075 2525 2975
Text Label 2500 2975 2    50   ~ 0
24
Entry Wire Line
	2625 3175 2525 3075
Text Label 2500 3075 2    50   ~ 0
26
Text Label 2500 3175 2    50   ~ 0
28
Wire Wire Line
	2525 1875 2350 1875
Wire Wire Line
	2525 1975 2350 1975
Wire Wire Line
	2525 2075 2350 2075
Wire Wire Line
	2525 2175 2350 2175
Wire Wire Line
	2525 2275 2350 2275
Wire Wire Line
	2525 2375 2350 2375
Wire Wire Line
	2525 2475 2350 2475
Wire Wire Line
	2525 2575 2350 2575
Wire Wire Line
	2525 2675 2350 2675
Wire Wire Line
	2525 2775 2350 2775
Wire Wire Line
	2525 2875 2350 2875
Wire Wire Line
	2525 2975 2350 2975
Wire Wire Line
	2525 3075 2350 3075
Wire Wire Line
	2525 3175 2350 3175
Entry Wire Line
	2675 1975 2775 1875
Text Label 2800 1875 0    50   ~ 0
1
Entry Wire Line
	2675 2075 2775 1975
Text Label 2800 1975 0    50   ~ 0
3
Entry Wire Line
	2675 2175 2775 2075
Text Label 2800 2075 0    50   ~ 0
5
Entry Wire Line
	2675 2275 2775 2175
Text Label 2800 2175 0    50   ~ 0
7
Entry Wire Line
	2675 2375 2775 2275
Text Label 2800 2275 0    50   ~ 0
9
Entry Wire Line
	2675 2475 2775 2375
Text Label 2800 2375 0    50   ~ 0
11
Entry Wire Line
	2675 2575 2775 2475
Text Label 2800 2475 0    50   ~ 0
13
Entry Wire Line
	2675 2675 2775 2575
Text Label 2800 2575 0    50   ~ 0
15
Entry Wire Line
	2675 2775 2775 2675
Text Label 2800 2675 0    50   ~ 0
17
Entry Wire Line
	2675 2875 2775 2775
Text Label 2800 2775 0    50   ~ 0
19
Entry Wire Line
	2675 2975 2775 2875
Text Label 2800 2875 0    50   ~ 0
21
Entry Wire Line
	2675 3075 2775 2975
Text Label 2800 2975 0    50   ~ 0
23
Entry Wire Line
	2675 3175 2775 3075
Text Label 2800 3075 0    50   ~ 0
25
Entry Wire Line
	2675 3275 2775 3175
Text Label 2800 3175 0    50   ~ 0
27
Entry Wire Line
	3725 1975 3625 1875
Text Label 3600 1875 2    50   ~ 0
2
Entry Wire Line
	3725 2075 3625 1975
Text Label 3600 1975 2    50   ~ 0
4
Entry Wire Line
	3725 2175 3625 2075
Text Label 3600 2075 2    50   ~ 0
6
Entry Wire Line
	3725 2275 3625 2175
Text Label 3600 2175 2    50   ~ 0
8
Entry Wire Line
	3725 2375 3625 2275
Text Label 3600 2275 2    50   ~ 0
10
Entry Wire Line
	3725 2475 3625 2375
Text Label 3600 2375 2    50   ~ 0
12
Entry Wire Line
	3725 2575 3625 2475
Text Label 3600 2475 2    50   ~ 0
14
Entry Wire Line
	3725 2675 3625 2575
Text Label 3600 2575 2    50   ~ 0
16
Entry Wire Line
	3725 2775 3625 2675
Text Label 3600 2675 2    50   ~ 0
18
Entry Wire Line
	3725 2875 3625 2775
Text Label 3600 2775 2    50   ~ 0
20
Entry Wire Line
	3725 2975 3625 2875
Text Label 3600 2875 2    50   ~ 0
22
Entry Wire Line
	3725 3075 3625 2975
Text Label 3600 2975 2    50   ~ 0
24
Entry Wire Line
	3725 3175 3625 3075
Text Label 3600 3075 2    50   ~ 0
26
Entry Wire Line
	3725 3275 3625 3175
Text Label 3600 3175 2    50   ~ 0
28
Wire Wire Line
	3625 1875 3450 1875
Wire Wire Line
	3625 1975 3450 1975
Wire Wire Line
	3625 2075 3450 2075
Wire Wire Line
	3625 2175 3450 2175
Wire Wire Line
	3625 2275 3450 2275
Wire Wire Line
	3625 2375 3450 2375
Wire Wire Line
	3625 2475 3450 2475
Wire Wire Line
	3625 2575 3450 2575
Wire Wire Line
	3625 2675 3450 2675
Wire Wire Line
	3625 2775 3450 2775
Wire Wire Line
	3625 2875 3450 2875
Wire Wire Line
	3625 2975 3450 2975
Wire Wire Line
	3625 3075 3450 3075
Wire Wire Line
	3625 3175 3450 3175
Entry Wire Line
	3775 1975 3875 1875
Text Label 3900 1875 0    50   ~ 0
1
Entry Wire Line
	3775 2075 3875 1975
Text Label 3900 1975 0    50   ~ 0
3
Entry Wire Line
	3775 2175 3875 2075
Text Label 3900 2075 0    50   ~ 0
5
Entry Wire Line
	3775 2275 3875 2175
Text Label 3900 2175 0    50   ~ 0
7
Entry Wire Line
	3775 2375 3875 2275
Text Label 3900 2275 0    50   ~ 0
9
Entry Wire Line
	3775 2475 3875 2375
Text Label 3900 2375 0    50   ~ 0
11
Entry Wire Line
	3775 2575 3875 2475
Text Label 3900 2475 0    50   ~ 0
13
Entry Wire Line
	3775 2675 3875 2575
Text Label 3900 2575 0    50   ~ 0
15
Entry Wire Line
	3775 2775 3875 2675
Text Label 3900 2675 0    50   ~ 0
17
Entry Wire Line
	3775 2875 3875 2775
Text Label 3900 2775 0    50   ~ 0
19
Entry Wire Line
	3775 2975 3875 2875
Text Label 3900 2875 0    50   ~ 0
21
Entry Wire Line
	3775 3075 3875 2975
Text Label 3900 2975 0    50   ~ 0
23
Entry Wire Line
	3775 3175 3875 3075
Text Label 3900 3075 0    50   ~ 0
25
Entry Wire Line
	3775 3275 3875 3175
Text Label 3900 3175 0    50   ~ 0
27
Wire Wire Line
	3875 1875 4050 1875
Wire Wire Line
	3875 1975 4050 1975
Wire Wire Line
	3875 2075 4050 2075
Wire Wire Line
	3875 2175 4050 2175
Wire Wire Line
	3875 2275 4050 2275
Wire Wire Line
	3875 2375 4050 2375
Wire Wire Line
	3875 2475 4050 2475
Wire Wire Line
	3875 2575 4050 2575
Wire Wire Line
	3875 2675 4050 2675
Wire Wire Line
	3875 2775 4050 2775
Wire Wire Line
	3875 2875 4050 2875
Wire Wire Line
	3875 2975 4050 2975
Wire Wire Line
	3875 3075 4050 3075
Wire Wire Line
	3875 3175 4050 3175
Entry Wire Line
	4825 1975 4725 1875
Text Label 4700 1875 2    50   ~ 0
2
Entry Wire Line
	4825 2075 4725 1975
Text Label 4700 1975 2    50   ~ 0
4
Entry Wire Line
	4825 2175 4725 2075
Text Label 4700 2075 2    50   ~ 0
6
Entry Wire Line
	4825 2275 4725 2175
Text Label 4700 2175 2    50   ~ 0
8
Entry Wire Line
	4825 2375 4725 2275
Text Label 4700 2275 2    50   ~ 0
10
Entry Wire Line
	4825 2475 4725 2375
Text Label 4700 2375 2    50   ~ 0
12
Entry Wire Line
	4825 2575 4725 2475
Text Label 4700 2475 2    50   ~ 0
14
Entry Wire Line
	4825 2675 4725 2575
Text Label 4700 2575 2    50   ~ 0
16
Entry Wire Line
	4825 2775 4725 2675
Text Label 4700 2675 2    50   ~ 0
18
Entry Wire Line
	4825 2875 4725 2775
Text Label 4700 2775 2    50   ~ 0
20
Entry Wire Line
	4825 2975 4725 2875
Text Label 4700 2875 2    50   ~ 0
22
Entry Wire Line
	4825 3075 4725 2975
Text Label 4700 2975 2    50   ~ 0
24
Entry Wire Line
	4825 3175 4725 3075
Text Label 4700 3075 2    50   ~ 0
26
Entry Wire Line
	4825 3275 4725 3175
Text Label 4700 3175 2    50   ~ 0
28
Wire Wire Line
	4725 1875 4550 1875
Wire Wire Line
	4725 1975 4550 1975
Wire Wire Line
	4725 2075 4550 2075
Wire Wire Line
	4725 2175 4550 2175
Wire Wire Line
	4725 2275 4550 2275
Wire Wire Line
	4725 2375 4550 2375
Wire Wire Line
	4725 2475 4550 2475
Wire Wire Line
	4725 2575 4550 2575
Wire Wire Line
	4725 2675 4550 2675
Wire Wire Line
	4725 2775 4550 2775
Wire Wire Line
	4725 2875 4550 2875
Wire Wire Line
	4725 2975 4550 2975
Wire Wire Line
	4725 3075 4550 3075
Wire Wire Line
	4725 3175 4550 3175
Entry Wire Line
	4875 1975 4975 1875
Text Label 5000 1875 0    50   ~ 0
1
Entry Wire Line
	4875 2075 4975 1975
Text Label 5000 1975 0    50   ~ 0
3
Entry Wire Line
	4875 2175 4975 2075
Text Label 5000 2075 0    50   ~ 0
5
Entry Wire Line
	4875 2275 4975 2175
Text Label 5000 2175 0    50   ~ 0
7
Entry Wire Line
	4875 2375 4975 2275
Text Label 5000 2275 0    50   ~ 0
9
Entry Wire Line
	4875 2475 4975 2375
Text Label 5000 2375 0    50   ~ 0
11
Entry Wire Line
	4875 2575 4975 2475
Text Label 5000 2475 0    50   ~ 0
13
Entry Wire Line
	4875 2675 4975 2575
Text Label 5000 2575 0    50   ~ 0
15
Entry Wire Line
	4875 2775 4975 2675
Text Label 5000 2675 0    50   ~ 0
17
Entry Wire Line
	4875 2875 4975 2775
Text Label 5000 2775 0    50   ~ 0
19
Entry Wire Line
	4875 2975 4975 2875
Text Label 5000 2875 0    50   ~ 0
21
Entry Wire Line
	4875 3075 4975 2975
Text Label 5000 2975 0    50   ~ 0
23
Entry Wire Line
	4875 3175 4975 3075
Text Label 5000 3075 0    50   ~ 0
25
Entry Wire Line
	4875 3275 4975 3175
Text Label 5000 3175 0    50   ~ 0
27
Wire Wire Line
	4975 1875 5150 1875
Wire Wire Line
	4975 1975 5150 1975
Wire Wire Line
	4975 2075 5150 2075
Wire Wire Line
	4975 2175 5150 2175
Wire Wire Line
	4975 2275 5150 2275
Wire Wire Line
	4975 2375 5150 2375
Wire Wire Line
	4975 2475 5150 2475
Wire Wire Line
	4975 2575 5150 2575
Wire Wire Line
	4975 2675 5150 2675
Wire Wire Line
	4975 2775 5150 2775
Wire Wire Line
	4975 2875 5150 2875
Wire Wire Line
	4975 2975 5150 2975
Wire Wire Line
	4975 3075 5150 3075
Wire Wire Line
	4975 3175 5150 3175
Entry Wire Line
	5925 1975 5825 1875
Text Label 5800 1875 2    50   ~ 0
2
Entry Wire Line
	5925 2075 5825 1975
Text Label 5800 1975 2    50   ~ 0
4
Entry Wire Line
	5925 2175 5825 2075
Text Label 5800 2075 2    50   ~ 0
6
Entry Wire Line
	5925 2275 5825 2175
Text Label 5800 2175 2    50   ~ 0
8
Entry Wire Line
	5925 2375 5825 2275
Text Label 5800 2275 2    50   ~ 0
10
Entry Wire Line
	5925 2475 5825 2375
Text Label 5800 2375 2    50   ~ 0
12
Entry Wire Line
	5925 2575 5825 2475
Text Label 5800 2475 2    50   ~ 0
14
Entry Wire Line
	5925 2675 5825 2575
Text Label 5800 2575 2    50   ~ 0
16
Entry Wire Line
	5925 2775 5825 2675
Text Label 5800 2675 2    50   ~ 0
18
Entry Wire Line
	5925 2875 5825 2775
Text Label 5800 2775 2    50   ~ 0
20
Entry Wire Line
	5925 2975 5825 2875
Text Label 5800 2875 2    50   ~ 0
22
Entry Wire Line
	5925 3075 5825 2975
Text Label 5800 2975 2    50   ~ 0
24
Entry Wire Line
	5925 3175 5825 3075
Text Label 5800 3075 2    50   ~ 0
26
Entry Wire Line
	5925 3275 5825 3175
Text Label 5800 3175 2    50   ~ 0
28
Wire Wire Line
	5825 1875 5650 1875
Wire Wire Line
	5825 1975 5650 1975
Wire Wire Line
	5825 2075 5650 2075
Wire Wire Line
	5825 2175 5650 2175
Wire Wire Line
	5825 2275 5650 2275
Wire Wire Line
	5825 2375 5650 2375
Wire Wire Line
	5825 2475 5650 2475
Wire Wire Line
	5825 2575 5650 2575
Wire Wire Line
	5825 2675 5650 2675
Wire Wire Line
	5825 2775 5650 2775
Wire Wire Line
	5825 2875 5650 2875
Wire Wire Line
	5825 2975 5650 2975
Wire Wire Line
	5825 3075 5650 3075
Wire Wire Line
	5825 3175 5650 3175
Entry Bus Bus
	1475 3550 1575 3450
Entry Bus Bus
	2575 3550 2675 3450
Entry Bus Bus
	3625 3550 3725 3450
Entry Bus Bus
	3675 3550 3775 3450
Entry Bus Bus
	4725 3550 4825 3450
Entry Bus Bus
	4775 3550 4875 3450
Entry Bus Bus
	5825 3550 5925 3450
Entry Wire Line
	2625 3275 2525 3175
Entry Wire Line
	1575 3275 1675 3175
Wire Wire Line
	2775 3175 2950 3175
Wire Wire Line
	2775 3075 2950 3075
Wire Wire Line
	2775 2975 2950 2975
Wire Wire Line
	2775 2875 2950 2875
Wire Wire Line
	2775 2775 2950 2775
Wire Wire Line
	2775 2675 2950 2675
Wire Wire Line
	2775 2575 2950 2575
Wire Wire Line
	2775 2475 2950 2475
Wire Wire Line
	2775 2375 2950 2375
Wire Wire Line
	2775 2275 2950 2275
Wire Wire Line
	2775 2175 2950 2175
Wire Wire Line
	2775 2075 2950 2075
Wire Wire Line
	2775 1975 2950 1975
Wire Wire Line
	2775 1875 2950 1875
$Comp
L Connector_Generic:Conn_02x15_Odd_Even J2
U 1 1 5FD3CA46
P 3150 2575
F 0 "J2" H 3200 3475 50  0000 C CNN
F 1 "MEASURE" H 3200 3375 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x15_Pitch2.54mm" H 3150 2575 50  0001 C CNN
F 3 "~" H 3150 2575 50  0001 C CNN
	1    3150 2575
	1    0    0    -1  
$EndComp
Entry Bus Bus
	2525 3550 2625 3450
Entry Wire Line
	1575 3375 1675 3275
Entry Wire Line
	2675 3375 2775 3275
Entry Wire Line
	3775 3375 3875 3275
Entry Wire Line
	4875 3375 4975 3275
Entry Wire Line
	2525 3275 2625 3375
Entry Wire Line
	3625 3275 3725 3375
Entry Wire Line
	4725 3275 4825 3375
Entry Wire Line
	5825 3275 5925 3375
Wire Wire Line
	1675 3275 1850 3275
Wire Wire Line
	2350 3275 2525 3275
Wire Wire Line
	2775 3275 2950 3275
Wire Wire Line
	3450 3275 3625 3275
Wire Wire Line
	3875 3275 4050 3275
Wire Wire Line
	4550 3275 4725 3275
Wire Wire Line
	4975 3275 5150 3275
Wire Wire Line
	5650 3275 5825 3275
Wire Bus Line
	1475 3550 5825 3550
Wire Bus Line
	1575 1975 1575 3450
Wire Bus Line
	2675 1975 2675 3450
Wire Bus Line
	3775 1975 3775 3450
Wire Bus Line
	4875 1975 4875 3450
Wire Bus Line
	2625 1975 2625 3450
Wire Bus Line
	3725 1975 3725 3450
Wire Bus Line
	4825 1975 4825 3450
Wire Bus Line
	5925 1975 5925 3450
Text Label 1700 3275 0    50   ~ 0
29
Text Label 2400 3275 0    50   ~ 0
30
Text Label 2800 3275 0    50   ~ 0
29
Text Label 3500 3275 0    50   ~ 0
30
Text Label 3900 3275 0    50   ~ 0
29
Text Label 4600 3275 0    50   ~ 0
30
Text Label 5000 3275 0    50   ~ 0
29
Text Label 5700 3275 0    50   ~ 0
30
$EndSCHEMATC
